import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../_services/_apiurl';
import { AnonymousSubject } from 'rxjs';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  users: any;
  constructor(public http: HttpClient) { }
  loading = false;
  ngOnInit() {
    this.loading = true;
    this.http.get(ApiUrl.BaseUrl + "User/userList").subscribe((res) => {
      this.users = res;
      this.loading = false;
    });
  }

}
