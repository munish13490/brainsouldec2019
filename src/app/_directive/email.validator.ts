import {
    ReactiveFormsModule,
    NG_VALIDATORS,
    FormsModule,
    FormGroup,
    FormControl,
    ValidatorFn,
    Validator,
    AbstractControl
} from '@angular/forms';
import { Directive } from '@angular/core';
@Directive({
    selector: '[emailvalidator][ngModel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: EmailValidator,
            multi: true
        }
    ]
})
export class EmailValidator implements Validator {
    validator: ValidatorFn;
    constructor() {
        this.validator = this.emailValidator();
    }
    validate(c: AbstractControl) {
        return this.validator(c);
    }

    emailValidator(): ValidatorFn {
        return (c: AbstractControl) => {
            let isValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(c.value);
            if (isValid) {
                return null;
            } else {
                return {
                    emailvalidator: {
                        valid: false
                    }
                };
            }
        }
    }
}