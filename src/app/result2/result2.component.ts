import { Component, OnInit, ChangeDetectorRef, Input, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../_services/_apiurl';
import * as Highcharts from 'highcharts';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-result2',
  templateUrl: './result2.component.html',
  styleUrls: ['./result2.component.css']
})
export class Result2Component implements OnInit {
  model = new ResultSecondLevel();
  chartConstructor: any;
  chartCallback: any;
  updateFlag: boolean = true;
  oneToOneFlag = true;
  data: any;
  L1: number = 0;
  L2: number = 0;
  R1: number = 0;
  R2: number = 0;
  constructor(private http: HttpClient, private _activatedRoute: ActivatedRoute, private changeDetectionRef: ChangeDetectorRef) { }

  Highcharts = Highcharts;
  @Input() UserId: any;
  @ViewChild('highchart') highchart: ElementRef;
  ngOnInit() {
    this.model.UserId = this.UserId;
    if (this.model.UserId != undefined) {
      this.http.post(ApiUrl.BaseUrl + 'Question/secondLevelAnswer', this.model).subscribe((res) => {
        this.data = res;
        this.highchart["chart"]["series"][0].setData(
          [
            {
              name: 'Strategist: ' + this.data.Strategist + '%',
              y: this.getChartSize(this.data.Strategist)

            },
            {
              name: 'Imaginer: ' + this.data.Imaginer + '%',
              y: this.getChartSize(this.data.Imaginer)
            },
            {
              name: 'Socialiser: ' + this.data.Socialiser + '%',
              y: this.getChartSize(this.data.Socialiser)
            },
            {
              name: 'Empathizer: ' + this.data.Empathizer + '%',
              y: this.getChartSize(this.data.Empathizer)
            },
            {
              name: 'Organiser: ' + this.data.Organiser + '%',
              y: this.getChartSize(this.data.Organiser)

            },
            {
              name: 'Preserver/ Stalwart: ' + this.data.PS + '%',
              y: this.getChartSize(this.data.PS)
            },
            {
              name: 'Realist: ' + this.data.Realist + '%',
              y: this.getChartSize(this.data.Realist)
            },
            {
              name: 'Analyst: ' + this.data.Analyst + '%',
              y: this.getChartSize(this.data.Analyst)
            }
          ]
        );

      }, error => console.error(error));
    }

  }



  chartOptions = {
    chart: {
      type: 'pie'
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
        }
      }
    },
    tooltip: {
      enabled: false
    },
    series: [{
      name: 'Brain Test Result',
      colorByPoint: true,
      data: [
        {
          name: 'Strategist: ' + this.L1,
          y: 45

        },
        {
          name: 'Imaginer: ' + this.R1,
          y: 45
        },
        {
          name: 'Socialiser: ' + this.R2,
          y: 45
        },
        {
          name: 'Empathizer: ' + this.L2,
          y: 45
        },
        {
          name: 'Organiser: ' + this.L1,
          y: 45

        },
        {
          name: 'Preserver/ Stalwart: ' + this.R1,
          y: 45
        },
        {
          name: 'Realist: ' + this.R2,
          y: 45
        },
        {
          name: 'Analyst: ' + this.L2,
          y: 45
        }
      ]
    }]

  }


  private getChartSize(number): number {
    return 90 * number / 100;
  }


}


export class ResultSecondLevel {


  public UserId: string;
  public Strategist: string;
  public Imaginer: string;
  public Socialiser: string;
  public Empathizer: string;
  public Organiser: string;
  public PS: string;
  public Realist: string;
  public Analyst: string;

}

