import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { HttpService } from '../_services/_httpservices';
import { ApiUrl } from '../_services/_apiurl';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Payment } from '../models/payment';
import { PromoCodes } from '../models/PromoCodes';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  model = new Payment();

  promocode = new PromoCodes();

  constructor(private _httpService: HttpService, protected sanitizer: DomSanitizer, private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.model.EnUserId = this.activatedRoute.snapshot.paramMap.get('id');
    this.model.OriginalAmount = 99.00;
    this.finalAmount = this.model.OriginalAmount.toString();
  }
  paymentUrl: SafeHtml;
  loading = false;
  message = "";
  finalAmount = "";
  processPayment() {
    this.loading = true;

    this.model.Amount = this.finalAmount == "" ? this.model.OriginalAmount : parseInt(this.finalAmount);

    this.model.Amount = this.model.Amount == undefined ? this.model.OriginalAmount : this.model.Amount;
    if (this.model.Amount != 0) {
      this._httpService.post(ApiUrl.BaseUrl + "payment/process", this.model).subscribe(res => {
        this.paymentUrl = this.sanitizer.bypassSecurityTrustHtml(res);
        setTimeout(this.postSSlPayment, 2000);
      });
    }
    else if (this.model.Amount === 0) {
      window.location.href = "http://test.brainsoulnyou.com/paymentresponse";
    }
  }
  postSSlPayment() {
    this.loading = false;

    let form: any = document.getElementsByName("f1");
    form[0].submit();
  }
  applyPromoCode() {
    if (this.promocode.Code.length >= 4) {
      this._httpService.post(ApiUrl.BaseUrl + "promocode/promoCodeDetail", this.promocode).subscribe(res => {
        if (res != null) {
          this.message = "";
          this.promocode.DiscountRate = res.DiscountRate;
          this.calculateFinalAmount();
        }
        else {
          this.message = "Invalid PromoCode!";
        }
      });
    }

  }

  calculateFinalAmount() {
    let discount = Math.round(this.model.OriginalAmount * this.promocode.DiscountRate) / 100;
    this.finalAmount = Math.floor(this.model.OriginalAmount - discount).toString();
    this.model.Amount = Math.floor(this.model.OriginalAmount - discount);
  }

}


