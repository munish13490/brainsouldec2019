export class ApiUrl {
  //Developement Url
  //public static BaseUrl: string = 'http://localhost:65050/api/';



  // Live URl

  public static BaseUrl: string = 'http://api.brainsoulnyou.com/api/';


  //Payment GateWay
  public static Staging: 'https://securegw-stage.paytm.in/theia/processTransaction';
  public static Production: 'https://securegw.paytm.in/theia/processTransaction';

}
