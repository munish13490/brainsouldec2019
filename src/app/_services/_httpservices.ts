import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/observable';
import { debug } from 'util';
@Injectable()
export class HttpService {
  constructor(private _http: Http) { }
  get(url: string): Observable<any> {
    return this._http.get(url).map((res) => res.json());
  }
  post(url: string, model: any): Observable<any> {
    return this._http.post(url, model).map((res) => res.json());
  }

  put(url: string, model: any): Observable<any> {
    return this._http.put(url, model).map((res) => res.json());

  }

  delete(url: string): Observable<any> {
    return this._http.delete(url).map(res => res.json());
  }

  PostImage(url: string, formdata: any, options: any): Observable<any> {
    return this._http.post(url, formdata, options).map((res) => res.json());
  }



}
