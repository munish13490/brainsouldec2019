import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
@Injectable()
export class LoginGurad implements CanActivate {
    constructor(private router: Router) {

    }
    canActivate() {
        var userId = (localStorage.getItem('tempUserId'));
        if (userId == null) {
            this.router.navigate(['/login'])

        }
        else {

            return true;

        }
    }
}