import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
@Injectable()
export class AuthGurad implements CanActivate {
    constructor(private router: Router) {

    }
    canActivate() {
        var userId = localStorage.getItem('temp');
        if (userId != null) {
            return true;
        }
        else {
            this.router.navigate(['/braintest'])
        }
    }
}