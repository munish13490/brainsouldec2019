import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule ,  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './Component/nav-menu/nav-menu.component';
import { HomeComponent } from './Component/home/home.component';
import { LoginComponent } from './Component/login/login.component';
import { VideosComponent } from './Component/videos/list/videos.component';
import { AddVideoComponent } from './Component/videos/add/addvideo.component';
import { QuestionsComponent } from './Component/questions/list/questions.component';
import { Assessment1Component } from './Component/questions/assessment1/assessment1.component';
import { QuestionnaireComponent } from './Component/questionnaire/questionnaire.component';
import { FetchDataComponent } from './Component/fetch-data/fetch-data.component';
import { HttpService } from './_services/_httpservices';
import { HttpModule } from '@angular/http';
import { EmailValidator } from './_directive/email.validator';
import { NgDatepickerModule } from 'ng2-datepicker';
import {TextMaskModule} from 'angular2-text-mask';
import { AuthGurad } from './_services/auth.guard';
import { Assessment2Component } from './questions/assessment2/assessment2.component';
import { ResultComponent } from './result/result.component';
import { ChartsModule } from 'ng2-charts';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';
import { UserlistComponent } from './userlist/userlist.component';
import { Result2Component } from './result2/result2.component';
import { LoginGurad } from './_services/login.guard';
import { LogoutComponent } from './logout/logout.component';
import { ListComponent } from './component/image/list/list.component';
import { PrivacyPolicyComponent } from './Component/privacy-policy/privacy-policy.component';
import { RefundComponent } from './Component/refund/refund.component';
import { TermsofuseComponent } from './Component/termsofuse/termsofuse.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentResponseComponent } from './payment-response/payment-response.component';
import { PaymentErrorComponent } from './payment-error/payment-error.component';
import { AboutComponent } from './Component/about/about.component';
import { GalleryComponent } from './Component/gallery/gallery.component';
import { BlogComponent } from './Component/blog/blog.component';
import { ContactComponent } from './Component/contact/contact.component';
import { HeaderComponent } from './Component/header/header.component';
import { FooterComponent } from './Component/footer/footer.component';
import { PromoCodeAddComponent } from './promocode/add/add.component';
import { PromoCodeListComponent } from './promocode/list/list.component';
import { registerLocaleData } from '@angular/common';
import { InstructionsComponent } from './Component/instructions/instructions.component';
import { GenerateLinkComponent } from './Component/generate-link/generate-link.component';
import { AddInstituteComponent } from './Component/add-institute/add-institute.component';
import { InstituteResultsComponent } from './Component/institute-results/institute-results.component';
import { QuotesComponent } from './Component/quotes/quotes.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    VideosComponent,
    AddVideoComponent,
    FetchDataComponent,
    QuestionsComponent,
    QuestionnaireComponent,
    Assessment1Component,
    EmailValidator,
    Assessment2Component,
    ResultComponent,
    UserlistComponent,
    Result2Component,
    LogoutComponent,
    ListComponent,
    PrivacyPolicyComponent,
    RefundComponent,
    TermsofuseComponent,
    PaymentComponent,
    PaymentResponseComponent,
    PaymentErrorComponent,
    AboutComponent,
    GalleryComponent,
    BlogComponent,
    ContactComponent,
    HeaderComponent,
    FooterComponent,
    PromoCodeAddComponent,
    PromoCodeListComponent,
    InstructionsComponent,
    GenerateLinkComponent,
    AddInstituteComponent,
    InstituteResultsComponent,
    QuotesComponent,
  ],
  imports: [
    TextMaskModule,
    ChartsModule, AngularHighchartsChartModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    NgDatepickerModule,
    FormsModule, HttpModule, ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'braintest', pathMatch: 'full' },
      { path: 'admin', component: LoginComponent },
      { path: 'institute/:id', component: InstructionsComponent },
      { path: 'videos', component: VideosComponent, canActivate: [LoginGurad] },
      { path: 'add-video', component: AddVideoComponent, canActivate: [LoginGurad] },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'questions', component: QuestionsComponent },
      { path: 'braintest', component: QuestionnaireComponent },
      { path: 'result', component: ResultComponent },
      { path: 'result-two', component: Result2Component },
      { path: 'about', component: AboutComponent },
      { path: 'home', component: HomeComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'gallery', component: GalleryComponent },
      { path: 'userlist', component: UserlistComponent, canActivate: [LoginGurad] },
      { path: 'login', component: LoginComponent },
      { path: 'assessment-one', component: Assessment1Component, canActivate: [AuthGurad] },
      { path: 'assessment-two', component: Assessment2Component, canActivate: [AuthGurad] },
      { path: 'images', component: ListComponent, canActivate: [LoginGurad] },
      { path: 'payment/:id', component: PaymentComponent },
      { path: 'paymentresponse', component: PaymentResponseComponent },
      { path: 'paymenterror', component: PaymentErrorComponent },
      { path: 'promoCodes', component: PromoCodeListComponent, canActivate: [LoginGurad] },
      { path: 'addPromoCode', component: PromoCodeAddComponent, canActivate: [LoginGurad] },
      { path: 'institute-list', component: GenerateLinkComponent, canActivate: [LoginGurad] },
      { path: 'addInstitute', component: AddInstituteComponent, canActivate: [LoginGurad] },
      { path: 'addInstitute/:id', component: AddInstituteComponent, canActivate: [LoginGurad] },
      { path: 'logout', component: LogoutComponent },
      { path: 'institute-result', component: InstituteResultsComponent, canActivate: [LoginGurad] },
      { path: 'quotes', component: QuotesComponent, canActivate: [LoginGurad] },
    ])
  ],
  providers: [HttpService, AuthGurad, LoginGurad],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
