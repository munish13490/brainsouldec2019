import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../_services/_apiurl';
import * as Highcharts from 'highcharts';
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  model = new ResultFirstLevel();
  chartConstructor: any;
  chartCallback: any;
  updateFlag: boolean = true;
  oneToOneFlag = true;
  data: any;
  Name: string = "";
  Email: string = "";
  Mobile: string = "";
  SummaryResults: string = "";
  CareerChoices: string = "";
  L1: number = 0;
  L2: number = 0;
  R1: number = 0;
  R2: number = 0;
  Lable1: string = "";
  Lable2: string = "";
  Lable3: string = "";
  Lable4: string = "";
  result: Array<number> = [];
  TempResult: TempResult[] = [];
  constructor(private http: HttpClient, private _activatedRoute: ActivatedRoute) { }

  datas: any[] = [];
  Highcharts = Highcharts;
  text = "";
  loading = false;
  ViewMore = false;
  Text1 = "";
  Text2 = "";
  Text3 = "";
  Text4 = "";

  VHQ = "";
  VLQ = "";
  SHQ = "";
  ChoiceText = "";
  @ViewChild('highchart') highchart: ElementRef;
  ngOnInit() {
    this.text = "View More";
    this._activatedRoute.queryParams
      .subscribe(params => {
        this.model.UserId = params.authCode;
      });
    if (this.model.UserId != undefined) {
      this.loading = true;

      this.http.post(ApiUrl.BaseUrl + 'Question/firstLevelAnswer', this.model).subscribe((res) => {
        this.data = res;
        this.getSummaryResult(this.data);
        this.Name = this.data.Name;
        this.Email = this.data.Email;
        this.Mobile = this.data.Mobile;
        this.VLQ = this.data.VLQ;
        this.VHQ = this.data.VHQ;
        this.SHQ = this.data.SHQ;
        this.Text1 = this.data.Text1;
        this.Text2 = this.data.Text2;
        this.Text3 = this.data.Text3;
        this.Text4 = this.data.Text4;
        this.ChoiceText = this.data.ChoiceText;
        this.result.push(this.data.L1);
        this.result.push(this.data.L2);
        this.result.push(this.data.R1);
        this.result.push(this.data.R1);
        this.result = this.result.sort((n1, n2) => n1 + n2);
        this.L1 = this.data.L1;
        this.L2 = this.data.L2;
        this.R1 = this.data.R1;
        this.R2 = this.data.R2;
        this.Lable1 = this.GetQuardant(0);
        this.Lable2 = this.GetQuardant(1);
        this.Lable3 = this.GetQuardant(2);
        this.Lable4 = this.GetQuardant(3);

        this.highchart["chart"]["series"][0].setData([

          {
            name: 'R1 :' + this.R1,
            y: 90,
            color: this.getColorCode(this.R1)
          }, {
            name: 'R2 :' + this.R2,
            y: 90,
            color: this.getColorCode(this.R2)
          }, {
            name: 'L2 :' + this.L2,
            y: 90,
            color: this.getColorCode(this.L2)
          },
          {
            name: 'L1 :' + this.L1,
            y: 90,
            color: this.getColorCode(this.L1)
          }
        ]
        );

      }, error => console.error(error));
      this.loading = false;
    }

  }



  chartOptions = {
    chart: {
      type: 'pie'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
        }
      }
    },
    title: {
      text: ''
    },
    subtitle: {
      text: ''
    },
    tooltip: {
      enabled: false
    },
    series: [{
      name: 'Brain Test Result',
      colorByPoint: true,
      data: [
        {
          name: 'L1: ' + this.L1,
          y: 90

        },
        {
          name: 'R1: ' + this.R1,
          y: 90
        },
        {
          name: 'R2: ' + this.R2,
          y: 90
        }, {
          name: 'L2: ' + this.L2,
          y: 90
        }
      ]
    }]

  }

  private getColorCode(number): string {
    let colorCode = '';
    if (number >= 115) {
      colorCode = '#008f51';
    } else if (number >= 100) {
      colorCode = '#d4fd73';
    } else if (number >= 85) {
      colorCode = '#fffe03';
    } else if (number >= 70) {
      colorCode = '#ff9300';
    } else {
      colorCode = '#f7010c';
    }
    return colorCode;
  }

  public GetQuardant(number): any {

    if (this.L1 == this.result[number]) {
      return "L1";
    }
    else if (this.L2 == this.result[number]) {
      return "L2";
    }
    else if (this.R1 == this.result[number]) {
      return "R1";
    }
    else if (this.R2 == this.result[number]) {
      return "R2";
    }
  }
  public ViewMoreOrLess() {
    this.ViewMore = !this.ViewMore;
    this.text = this.ViewMore == true ? "View Less" : "View More";
  }

  DownloadPDf() {
    window.print();
    window.close();
  }

  getSummaryResult(data) {
    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    let Name = data.Name;
    console.log(data)
    //for all quadrants having high prefernce
    if (L1 >= 100 && L2 >= 100 && R1 >= 100 && R2 >= 100) {
      this.SummaryResults = "Congratulations " + Name + "...!" + " Dear " + Name + " you are whole brain person. This very rare combination as you have equal dominance in each quadrant.You are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task.You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player. You are perfectly fine for any career selection or subject selection, the only thing we prefer you to do is, you always have to be very focused about your Outcome, your Goal making and selection.";
    }
    //for high or very high preference in one quadrant
    else if (L1 >= 100 && L2 < 100 && R1 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have dominance only in 1 quadrant that is L1. You are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions."
      this.getCareerOfL1(data)
    }
    else if (L2 >= 100 && L1 < 100 && R1 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have dominance only in 1 quadrant that is L2. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task."
      this.getCareerOfL2(data)
    }
    else if (R1 >= 100 && L1 < 100 && L2 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have dominance only in 1 quadrant that is R1. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks.  You often function as strategists, designers, trendsetters and entrepreneurs"
      this.getCareerOfR1(data)
    }
    else if (R2 >= 100 && L1 < 100 && L2 < 100 && R1 < 100) {
      this.SummaryResults = "Dear " + Name + "  you have dominance only in 1 quadrant that is R2. You are people focused, caring, and sensitive and team player."
      this.getCareerOfR2(data)
    }
    //for combination of two
    else if (L1 >= 100 && L2 >= 100 && R1 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 and L2, you are objective, realist person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary."
    }
    else if (L1 >= 100 && R1 >= 100 && L2 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 and R1, you are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs."
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist."
    }
    else if (L1 >= 100 && R2 >= 100 && R1 < 100 && L2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 and R2, you are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    else if (L2 >= 100 && R2 >= 100 && R1 < 100 && L1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L2 and R2, you are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task. You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (L2 >= 100 && R1 >= 100 && L1 < 100 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L2 and R1, you are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task.You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs"
      this.CareerChoices = " Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (R1 >= 100 && R2 >= 100 && L1 < 100 && L2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R1 and R2, you prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks.  You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, the arts/entertainment, Paediatrician."
    }
    else {
      this.getCombinationOfThree(data)
    }
  }

  //get  data when only quadrant L1
  getCareerOfL1(data) {
    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    if (L2 > R1 && L2 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary"
    }
    else if (R1 > L2 && R1 > R2) {
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist."
    }
    else if (R2 > L2 && R2 > R1) {
      this.CareerChoices = "Any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    else if (R2 == L2 && R2 == R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist,Architect, Strategist, Trendsetter, Multimedia designer, Economist, any field of interest associated with L1 (engineering,financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    //when very high and two averages are equal
    else if (L1 >= 115 && L2 == R1 && L2 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist"
    }
    else if (L1 >= 115 && L2 == R2 && R2 > R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    else if (L1 < 115 && L2 == R1 && L2 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L1 < 115 && L2 == R2 && R2 > R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist"
    }

  }

  getCareerOfL2(data) {
    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    if (L1 > R1 && L1 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary."
    }
    else if (R1 > L1 && R1 > R2) {
      this.CareerChoices = "Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (R2 > L1 && R2 > R1) {
      this.CareerChoices = "Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (R2 == L1 && R2 == R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Project manager, Movie producer, Chef, Fashion designer, Software developer, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    //when very high and two averages are equal
    else if (L2 >= 115 && L1 == R1 && L1 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L2 >= 115 && L1 == R2 && L1 > R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (L2 < 115 && L1 == R1 && L1 > R2) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L2 < 115 && L1 == R2 && L1 > R1) {
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }

  }

  getCareerOfR1(data) {
    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    if (L1 > R2 && L1 > L2) {
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist."
    }
    else if (R2 > L2 && R2 > L1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, the arts/entertainment, Paediatrician."
    }
    else if (L2 > L1 && L2 > R2) {
      this.CareerChoices = "Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L2 == L1 && L2 == R2) {
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect,Strategist, Trendsetter, Multimedia designer, Economist, Selling, Public relations, Marketing, Training &amp; development,Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer,Software developer"
    }

    //when very high and two averages are equal
    else if (R1 >= 115 && R2 == L1 && R2 > L2) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist."
    }
    else if (R1 >= 115 && R2 == L2 && R2 > L1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (R1 < 115 && R2 == L1 && R2 > L1) {
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician"
    }
    else if (R1 < 115 && L2 == R2 && L2 > L1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer, Software developer, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }

  }

  getCareerOfR2(data) {

    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    if (L2 > R1 && L2 > L1) {
      this.CareerChoices = "Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (R1 > L2 && R1 > L1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, the arts/entertainment, Paediatrician."
    }
    else if (L1 > L2 && L1 > R1) {
      this.CareerChoices = "Any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    else if (L1 == L2 && L1 == R1) {
      this.CareerChoices = "Any field of interest associated with L1 (engineering, financial,scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Selling, Public relations,Marketing, Training &amp; development, Therapist, Tourism, The arts/entertainment, Paediatrician, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    //when very high and two averages are equal
    else if (R2 >= 115 && R1 == L1 && R1 > L2) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, the arts/entertainment, Paediatrician, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing"
    }
    else if (R2 >= 115 && L2 == R1 && L2 > R1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (R2 >= 115 && R1 == L1 && R1 > L2) {
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician."
    }
    else if (R2 >= 115 && L2 == R1 && L2 > R1) {
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer, Software developer, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }

  }

  //Combination of three
  getCombinationOfThree(data) {
    let L1 = data.L1;
    let L2 = data.L2;
    let R1 = data.R1;
    let R2 = data.R2;
    let Name = data.Name;
    //for combo of Very High+high+high
    if (L1 >= 115 && L2 >= 100 && L2 < 115 && R1 >= 100 && R1 < 115 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 & L2 and L1 & R1. You are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task.You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. "
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist."
    }
    else if (L1 >= 115 && L2 >= 100 && L2 < 115 && R2 >= 100 && R2 < 115 && R1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 & L2 and L1 & R2. You are realist and analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task. You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing."
    }
    else if (L2 >= 115 && L1 >= 100 && L1 < 115 && R1 >= 100 && R1 < 115 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L2 & L1 and L2 & R1. You are objective, realist person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L2 >= 115 && L1 >= 100 && L1 < 115 && R2 >= 100 && R2 < 115 && R1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L2 & L1 and L2 & R2. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task.You are realist and analyst person also who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions. You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (R1 >= 115 && R2 >= 100 && R2 < 115 && L1 >= 100 && L1 < 115 && L2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R1 & R2 and R1 & L1. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player. You are objective, realist person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist.";
    }
    else if (R1 >= 115 && R2 >= 100 && R2 < 115 && L2 >= 100 && L2 < 115 && L1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R1 & R2 and R1 & L2. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks.  You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (R2 >= 115 && R1 >= 100 && R1 < 115 && L1 >= 100 && L1 < 115 && L2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R2 & R1 and R2 & L1. You are people focused, caring, and sensitive and team player. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. You are realist & analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, the arts/entertainment, Paediatrician, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing.";
    }
    else if (R2 >= 115 && R1 >= 100 && R1 < 115 && L2 >= 100 && L2 < 115 && L1 < 100) {
      this.SummaryResults = "Dear " + Name + "you have a combination of R2 & R1 and R2 & L2. You are people focused, caring, and sensitive and team player. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks.  You often function as strategists, designers, trendsetters and entrepreneurs. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    //When combination is high+high+high
    else if (L1 >= 100 && L1 < 115 && L2 >= 100 && L2 < 115 && R1 >= 100 && R1 < 115 && R2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 & L2, L1 & R1, L2 & R1. You are realist & analyst person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task.You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks.  You often function as strategists, designers, trendsetters and entrepreneurs."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, Project manager, Movie producer, Chef, Fashion designer, Software developer."
    }
    else if (L1 >= 100 && L1 < 115 && L2 >= 100 && L2 < 115 && R2 >= 100 && R2 < 115 && R1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of L1 & L2, L1 & R2, L2 & R2. You are objective, realist person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions.You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task. You are people focused, caring, and sensitive and team player."
      this.CareerChoices = "Engineer, Surgeon, Accountant, Computer analyst, Commercial pilot, financial analyst, Statistician, Administrator, Lawyer, Actuary, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else if (R1 >= 100 && R1 < 115 && R2 >= 100 && R2 < 115 && L1 >= 100 && L1 < 115 && L2 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R1 & R2, R1 & L1, R2 & L1. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player. You are objective, realist person who likes to dig deeper to find the essence of problems and solutions. You like to do things well, to avoid mistakes and to focus on the job at hand. You often fulfill financial, analytical, technical or research functions"
      this.CareerChoices = "Researcher, Design engineer, Graphic artist, Architect, Strategist, Trendsetter, Multimedia designer, Economist, any field of interest associated with L1 (engineering, financial, scientific, research, etc.) in positions like: Training consultant, Advisor, Sales, Marketing, Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician."
    }
    else if (R1 >= 100 && R1 < 115 && R2 >= 100 && R2 < 115 && L2 >= 100 && L2 < 115 && L1 < 100) {
      this.SummaryResults = "Dear " + Name + " you have a combination of R1 & R2, R1 & L2, R2 & L2. You prefer to big picture thinking, to probe and question, to function in more unstructured environments and to sometimes take risks. You often function as strategists, designers, trendsetters and entrepreneurs. You are people focused, caring, and sensitive and team player. You are doer who prefers to follow procedures of a systematic approach. You are the organizer, are reliable and would persevere with a task."
      this.CareerChoices = "Selling, Public relations, Marketing, Training & development, Therapist, Tourism, The arts/entertainment, Paediatrician, Project manager, Movie producer, Chef, Fashion designer, Software developer, Nurse, Social worker, Paramedic, Kindergarten teacher, Management, Volunteer, Secretary, Supervisor, Client service specialist."
    }
    else {
      this.SummaryResults = "Something went wrong";
    }
  }
}


export class ResultFirstLevel {

  public UserId: string;
  public Name: string;
  public Email: string;
  public Mobile: string;
  public L1: number;
  public L2: number;
  public R1: number;
  public R2: number;

}

export class TempResult {
  public key: string;
  public Value: number;

}

