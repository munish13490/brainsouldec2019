import { Component, OnInit } from '@angular/core';
import { PromoCodes } from '../../models/PromoCodes';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../../_services/_apiurl';
import { Router } from '@angular/router';
@Component({
  selector: 'promocode-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class PromoCodeAddComponent implements OnInit {

  model = new PromoCodes();
  loading = false;
  message = "";
  addOrEditMatDialog = "";
  constructor(public http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  submit() {
    this.loading = true;
    this.http.post(ApiUrl.BaseUrl + "promocode/addPromoCode", this.model).subscribe((res) => {
      this.loading = false;
      if (res == true) {
        this.router.navigate(['/promoCodes']);
      }
      else {
        this.message = "Promo code alreay exist";
      }
    });
  }

}
