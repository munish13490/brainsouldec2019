import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../../_services/_apiurl';
@Component({
  selector: 'promocode-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PromoCodeListComponent implements OnInit {

  constructor(public http: HttpClient) { }
  promoCodes: any;
  loading = false;
  ngOnInit() {
    this.loading = true;
    this.http.get(ApiUrl.BaseUrl + "promocode/promoCodes").subscribe((res) => {
      this.promoCodes = res;
      this.loading = false;
    });
  }

  deletePromoCode(item: any) {
    this.loading = true;
    this.http.post(ApiUrl.BaseUrl + "promocode/deletePromoCodes", item).subscribe((res) => {
      this.loading = false;
      this.ngOnInit();
    });
  }


}
