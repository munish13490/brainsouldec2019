import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../../_services/_apiurl';
import { debug } from 'util';
@Component({
    selector: 'app-assessment2',
    templateUrl: './assessment2.component.html',
    styleUrls: ['./assessment2.component.css']
})
export class Assessment2Component implements OnInit {
    questions;
    showQusetion = false;
    loading = false;
    errorMeassage = "";

    constructor(private http: HttpClient) { }
    AnswerModel = new AnswerList();
    ngOnInit() {
        this.http.get(ApiUrl.BaseUrl + 'Question/SecondAssesement').subscribe((res) => {
            this.questions = res;
        }, error => console.error(error));
    }
    Submit() {
        if (this.AnswerModel.AssesementAnswers.length < 8) {
            this.errorMeassage = "Please attempt all questions with all options to get accurate result.";
        }

        else {
            var detail = this.AnswerModel.AssesementAnswers.find(x => x.Options.length < 4);
            if (detail != undefined || detail != null) {
                this.errorMeassage = "Please attempt all questions with all options to get accurate result.";
            }
            else {
                this.loading = true;
                this.errorMeassage = "";
                var UserId = parseInt(localStorage.getItem("temp")) == NaN ? 1 : parseInt(localStorage.getItem("temp"));
                this.AnswerModel.UserId = UserId;
                this.http.post(ApiUrl.BaseUrl + "Question/addSecondAssesementAnswer", this.AnswerModel).subscribe((res) => {
                    this.loading = false;
                    this.errorMeassage = "";
                    if (res == true) {
                        localStorage.removeItem('temp');
                        this.showQusetion = true;

                    }
                });
            }
        }


    }

    handleChange(event: any, optionId: any, questionId: any, answerCode: any) {
        if (event.target.checked) {
            var data = this.AnswerModel.AssesementAnswers.find(x => x.QuestionId == questionId);
            if (data != undefined) {
                var subdata = data.Options.find(x => x.optionId == optionId);
                if (subdata != undefined) {
                    subdata.AnwerCode = answerCode;

                }
                else {
                    var optionModel = new option();
                    optionModel.AnwerCode = answerCode;
                    optionModel.optionId = optionId;
                    data.Options.push(optionModel);
                }
            }

            else {
                var model = new AssesementAnswer();
                model.QuestionId = questionId;
                var optionModel = new option();
                optionModel.AnwerCode = answerCode;
                optionModel.optionId = optionId;
                model.Options.push(optionModel);
                this.AnswerModel.AssesementAnswers.push(model);
            }


        }
    }


}
export class AnswerList {
    public UserId: number;
    public AssesementAnswers: AssesementAnswer[] = [];
}
export class AssesementAnswer {
    public QuestionId: number;
    public Options: option[] = [];
}

export class option {
    public optionId: number;
    public AnwerCode: string;
}