import { Component, OnInit } from '@angular/core';
import { ApiUrl } from '../../../_services/_apiurl';
import { HttpService } from '../../../_services/_httpservices';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(public http: HttpService) { }
  public files: any[];
  Images = [];
  loading = false;
  pic;
  ngOnInit() {
    this.getImages();
  }
  onFileChanged(event: any) {
    this.files = event.target.files;
  }
  getImages() {
    this.loading = true;
    this.http.get(ApiUrl.BaseUrl + "Videos/images").subscribe(res => {
      this.loading = false;
      this.Images = res;
    });
  }
  Delete(data: any) {
    this.loading = true;
    this.http.post(ApiUrl.BaseUrl + "Videos/deleteImages", data).subscribe(res => {
      this.loading = false;
      this.getImages();
    });
  }
  setDefaultPic() {
    this.pic = "assets/img/logo.png";
  }
  onUpload() {
    this.loading = true;
    const formData = new FormData();
    for (const file of this.files) {
      formData.append(name, file, file.name);
    }
    if (this.files.length > 0) {
      this.http.post(ApiUrl.BaseUrl + "Videos/uploadImages", formData).subscribe(res => {
        this.loading = false;
        if (res == 201) {
          this.getImages();
        }
      });
    }
  }
}
