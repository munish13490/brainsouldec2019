import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Institute } from '../../models/institute';
import { ApiUrl } from '../../_services/_apiurl';

@Component({
  selector: 'app-institute-results',
  templateUrl: './institute-results.component.html',
  styleUrls: ['./institute-results.component.css']
})
export class InstituteResultsComponent implements OnInit {
  loading: boolean;
  institutionsList: Institute[] = [];
  instituteSelected: number;
  submitted: boolean;
  users: any = [];
  notFound: boolean;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: Router) { }
  ngOnInit() {
    this.getInstituteList();
  }

  getInstituteList() {
    this.loading = true;
    this.http.get<Institute[]>(ApiUrl.BaseUrl + "Institution/GetInstitutions").subscribe(res => {
      this.loading = false;
      this.institutionsList = res;
    },
    error => this.loading = false);
  }

  getResults() {
    this.submitted = true;
    this.notFound = false;
    this.loading = true;
    if (!this.instituteSelected)
      return
    this.http.get(ApiUrl.BaseUrl + "User/GetInstitutionUser?institutionId=" + this.instituteSelected).subscribe(res => {
      this.loading = false;
      this.users = res
    },
      error => {
        this.users = [];
        if (error.statusText === "Not Found") {
          this.notFound = true;
          this.loading =false
        }
      });
  }

}
