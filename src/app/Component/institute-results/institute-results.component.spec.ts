import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstituteResultsComponent } from './institute-results.component';

describe('InstituteResultsComponent', () => {
  let component: InstituteResultsComponent;
  let fixture: ComponentFixture<InstituteResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstituteResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstituteResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
