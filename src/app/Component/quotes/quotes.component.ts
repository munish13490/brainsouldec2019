import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiUrl } from '../../_services/_apiurl';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  public files: any[];
  loading: boolean;
  imgURL: any;
  public imagePath;
  id = '';
  quotes: any = [];

  constructor(private router: Router, private http: HttpClient) { }


  ngOnInit() {
    this.loading = true;
    this.getQuotes();
  }

  getQuotes() {
    this.http.get(ApiUrl.BaseUrl + "Quotes/GetQuotesImages").subscribe(res => {
      this.loading = false;
      this.quotes = res
    },
      () => this.loading = false);
  }

  deleteQuote(imgId) {
    this.loading = true;
    this.http.post(ApiUrl.BaseUrl + "Quotes/DeleteQuotesImage/" + imgId, {}).subscribe(res => {
      this.loading = false;
      this.getQuotes();
    });
  }

  onFileChanged(event: any) {
    this.files = event.target.files;
    var reader = new FileReader();
    this.imagePath = this.files;
    reader.readAsDataURL(this.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  onUpload() {
    this.loading = true;
    const formData = new FormData();
    for (const file of this.files) {
      formData.append("file", file, file.name);
    }
    if (this.files.length > 0) {
      this.http.post(ApiUrl.BaseUrl + "Quotes/AddQuotesImage", formData).subscribe(res => {
        this.loading = false;
        if (res) {
          this.getQuotes();
        }
      },
        () => this.loading = false);
    }
  }
}
