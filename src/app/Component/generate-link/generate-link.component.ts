import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Institute } from '../../models/institute';
import { ApiUrl } from '../../_services/_apiurl';

@Component({
  selector: 'app-generate-link',
  templateUrl: './generate-link.component.html',
  styleUrls: ['./generate-link.component.css']
})
export class GenerateLinkComponent implements OnInit {
  loading = false;
  institutionsList: Institute[] = [];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: Router) { }

  ngOnInit() {
    this.getInstituteList();
  }


  getInstituteList() {
    this.loading = true;
    this.http.get<Institute[]>(ApiUrl.BaseUrl + "Institution/GetInstitutions").subscribe(res => {
      this.loading = false;
      this.institutionsList = res;
    },
      () => this.loading = false);
  }

  editInstitute(institute: Institute) {
    this.router.navigate(['/addInstitute/' + institute.Id])
  }


  deleteCollegeFromList(institute: Institute) {
    const index = this.institutionsList.indexOf(institute)
    this.loading = true;
    const id = {
      Id: institute.Id
    }
    this.http.post(ApiUrl.BaseUrl + "Institution/DeleteInstitution", id).subscribe(res => {
      if (res) {
        this.loading = false;
        this.institutionsList.splice(index, 1)
      }
    },
      () => this.loading = false
    )
  }

}
