import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Institute } from '../../models/institute';
import { InstitutesForm } from '../../models/InstitutesForm';
import { ApiUrl } from '../../_services/_apiurl';

@Component({
  selector: 'app-add-institute',
  templateUrl: './add-institute.component.html',
  styleUrls: ['./add-institute.component.css']
})
export class AddInstituteComponent implements OnInit {
  model = new InstitutesForm();
  loading = false;
  message: string;
  id='';

  constructor(private route: ActivatedRoute , private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  submit() {
    this.loading = true;
    if(!this.id){
    const addPatient: Institute={
      Name:this.model.collegeName,
      Url:this.model.collegeUrl.toLowerCase(),
      TypeId: 3
    }
    this.http.post(ApiUrl.BaseUrl + "Institution/AddInstitution", addPatient).subscribe(res => {
      this.loading = false;
      if(res === true)
      this.router.navigate(['/institute-list'])
    else
    this.message = "Institution already exists."
    },
    ()=> this.loading = false);
  }
  else{
    const editPatient: Institute={
      Id:+this.id,
      Name:this.model.collegeName,
      Url:this.model.collegeUrl.toLowerCase(),
      TypeId: 3
    }
    this.http.post(ApiUrl.BaseUrl + "Institution/UpdateInstitution", editPatient).subscribe(res => {
      this.loading = false; 
      if(res === true)
        this.router.navigate(['/institute-list'])
      else
      this.message = "Institution already exists."
    },
    ()=>this.loading = false);
  }
  }

}