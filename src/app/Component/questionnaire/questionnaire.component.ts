import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../../_services/_apiurl';
import { HttpHeaders } from '@angular/common/http';
import { DatepickerOptions } from 'ng2-datepicker';
import 'rxjs/add/operator/map';
import { User } from '../../models/user';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'questionnaire-component',
  templateUrl: './questionnaire.component.html'
})


export class QuestionnaireComponent implements OnInit {
  model = new User();

  submitted = false;
  status: boolean;
  BrainTestCode;
  loading = false;
  constructor(private _activatedRoute: ActivatedRoute, private _route: Router,
    public http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }
  Date;
  code = "";
  errorMessage = "";
  showCodeDiv = false;
  submodel: any;
  mesaage = "";
  @ViewChild('f2') form: any;
  ngOnInit() {
    this.model = new User();
    this.mesaage = "";
    this._activatedRoute.queryParams
      .subscribe(params => {
        this.code = params.authId;
      });
    if (this.code != null) {
      this.model.BrainTestCode = this.code
      this.validateId();
    }


  }
  ValidateCode() {
    this.http.post(ApiUrl.BaseUrl + "User/validateCode", this.model).subscribe((res) => {
      if (res != 0) {
        this.errorMessage = "";
        this._route.navigateByUrl('assessment-one');
        localStorage.setItem("temp", res.toString());
      }
      else {
        this.errorMessage = "Please enter valid code";
      }
    });
  }
  validateId() {
    this.http.post(ApiUrl.BaseUrl + "User/validateCode", this.model).subscribe((res) => {
      if (res != 0) {
        this.model.BrainTestCode = null;
        this.showCodeDiv = true;
      }
      else {
        this.model.BrainTestCode = null;
        this.showCodeDiv = false;
      }
    });

  }

  options: DatepickerOptions = {
    minYear: (new Date().getFullYear() - 50),
    maxYear: new Date().getFullYear(),
    displayFormat: '  DD[/] MM/YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0,
    maxDate: new Date(),
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Click to select a date', // HTML input placeholder attribute (default: '')
    addClass: 'form-control calender', // Optional, value to pass on to [ngClass] on the input field
    addStyle: { NgStyle: 'z-index:999' }, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  showdetailform = true;
  onSubmit(ngForm: NgForm) {
    this.loading = true;
    this.submitted = true;
    this.submodel = this.model;
    this.http.post(ApiUrl.BaseUrl + "User/AddUser", this.submodel).subscribe((res) => {
      this.loading = false
      if (res != null) {
        this._route.navigate(['/payment', res]);

      }

    });
  }
}

