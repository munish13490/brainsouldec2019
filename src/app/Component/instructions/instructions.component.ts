import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Institute } from '../../models/institute';
import { ApiUrl } from '../../_services/_apiurl';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.css']
})
export class InstructionsComponent implements OnInit {
  genders: any[] = [{
    id: 1, name: "Male"
  }, {
    id: 2, name: "Female"
  },
  {
    id: 3, name: "Unspecified"
  },
  ];

  userForm = '';
  formValue: any;
  instituteForm: FormGroup;
  institute: Institute;
  instituteUrl: string;
  submitted = false;
  loading = false;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private http: HttpClient , private router: Router) {
    this.instituteUrl = this.route.snapshot.paramMap.get('id');
    if (this.instituteUrl)
      this.getInstituteByUrl(this.instituteUrl)
  }

  ngOnInit() {
    this.instituteForm = this.fb.group({
      studentName: ['', Validators.required],
      branch: ['', Validators.required],
      year: ['', Validators.required],
      rollNumber: ['', Validators.required],
      gender: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      email: [''],
      age: [''],
    });
  }

  get f() { return this.instituteForm.controls; }


  onsubmit() {
    this.submitted = true;
    if (this.instituteForm.invalid)
      return
    this.loading = true;
    const addUser = {
      Name: this.instituteForm.value.studentName,
      Age: this.instituteForm.value.age,
      Gender: this.instituteForm.value.gender,
      Mobile: this.instituteForm.value.phoneNumber,
      email: this.instituteForm.value.email,
      DOB: "",
      Type: this.institute.TypeId,
      InstitutionId: this.institute.Id,
      Branch: this.instituteForm.value.branch,
      Year: this.instituteForm.value.year,
      RollNumber: this.instituteForm.value.rollNumber,
      ProfileType: "Student"
    }
    this.http.post(ApiUrl.BaseUrl + "User/AddUser", addUser).subscribe(res => {
      this.loading = false;
      if(res!= 0){
      localStorage.setItem("temp", res.toString());
      this.router.navigateByUrl('assessment-one');
      }
    },
      () => this.loading = false);
  }

  getInstituteByUrl(url: string) {
    this.loading = true;
    this.http.get<Institute>(ApiUrl.BaseUrl + "Institution/GetInstitutionByUrl?url=" + url).subscribe(res => {
      this.loading = false
      this.institute = res;
    },
      () => this.loading = false);
  }


}

