import { Component, OnInit, ViewChild } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { HttpService } from '../../_services/_httpservices';
import { ApiUrl } from '../../_services/_apiurl';
import { ContactUs } from '../../models/ContactUs';
import { DatepickerOptions } from 'ng2-datepicker';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  model = new ContactUs();
  date;
  message = "";
  constructor(private sanitizer: DomSanitizer, private _http: HttpService) {

  }
  @ViewChild('f') form: any;
  options: DatepickerOptions = {
    minYear: (new Date().getFullYear() - 50),
    maxYear: new Date().getFullYear(),
    displayFormat: '  DD[/] MM/YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0,
    maxDate: new Date(),
    barTitleIfEmpty: 'Click to select a date',
    placeholder: 'Click to select a date', // HTML input placeholder attribute (default: '')
    addClass: 'form-control calender', // Optional, value to pass on to [ngClass] on the input field
    addStyle: { NgStyle: 'z-index:999' }, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };
  videoUrl: SafeResourceUrl;
  loading = false;
  totalVisit: any;
  Images = [];
  imagesLength;
  ngOnInit() {

    this.getVideoUrl();
    this.model.Message = "";
  }
  Name = '';
  getTotalVisit() {
    this._http.post(ApiUrl.BaseUrl + "User/WebsiteVisit", null).subscribe((res) => {
      this.totalVisit = res;
      this.getImages();
    });
  }
  getImages() {
    this._http.get(ApiUrl.BaseUrl + "Videos/images").subscribe(res => {
      this.Images = res;
      if (this.Images.length > 0) {
        this.imagesLength = this.Images.length;

      }
      else {
        this.imagesLength = 0;
      }
    });
  }
  getVideoUrl() {
    this._http.get(ApiUrl.BaseUrl + "Videos/GetVideos").subscribe((res) => {
      if (res != null || res != undefined) {
        let tempUrl = res[0].Url;
        if (tempUrl.includes("watch?v=")) {
          tempUrl = tempUrl.replace('watch?v=', 'embed/');

        }
        this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(tempUrl);
        this.getTotalVisit();
      }

    });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  contactUs() {
    this.loading = true;
    this._http.post(ApiUrl.BaseUrl + "Contactus/contactUs", this.model).subscribe((res) => {
      this.loading = false;
      if (res != null) {
        this.message = "You have registered successfully. You will get confirmation by Email.";
        this.form.submitted = false;
        this.form.reset();
      }


    });
  }
}
