import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'questions-component',
  templateUrl: './questions.component.html'
})
export class QuestionsComponent {
  public questions: Question[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Question[]>(baseUrl + 'api/Question/GetQuestions').subscribe(result => {
      this.questions = result;
    }, error => console.error(error));
  }
}

interface Question {
  id: number;
  question: string;
  answer1: string;
  answerCode1: string;
  answer2: string;
  answerCode2: string;
  answer3: string;
  answerCode3: string;
  answer4: string;
  answerCode4: string;
  isActive: boolean;
}
