import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { ApiUrl } from '../../../_services/_apiurl';
import { Router } from '@angular/router';
import { HttpService } from '../../../_services/_httpservices';

@Component({
  selector: 'assessment1-component',
  templateUrl: './assessment1.component.html'
})
export class Assessment1Component implements OnInit {
  public questions = [];
  loading = false;
  errorMeassage = "";
  constructor(private http: HttpClient, private _httpService: HttpService, private _route: Router) {
    _httpService.get(ApiUrl.BaseUrl + 'Question/GetQuestions').subscribe(result => {
      this.questions = result;
    }, error => console.error(error));
  }
  ngOnInit() {


  }
  answerModel = new SelectedAnswer();



  AnswerClick(event: any, id: number, order: any, orderSequence: number) {
    if (event.target.checked) {
      var data = this.answerModel.Questionlist.find(x => x.id == id);
      if (data != undefined) {
        var item = new AnswerItem();
        item.order = data.AnswerItems.length + 1;
        item.AnwserCode = event.target.id;
        data.AnswerItems.push(item);
        var detail2 = this.questions.filter(y => y.Id == id);
        var detail = detail2[0];
        switch (order) {
          case 1:
            detail.AnswerOrder1 = item.order;
            break;
          case 2:
            detail.AnswerOrder2 = item.order;
            break;
          case 3:
            detail.AnswerOrder3 = item.order;
            break;
          default:
            detail.AnswerOrder4 = item.order;
        }
      }
      else {
        var question = new QuestionsAnswer();
        question.id = id;
        question.AnswerItems = [];
        var item2 = new AnswerItem();
        item2.AnwserCode = event.target.id;
        item2.order = 1;
        question.AnswerItems.push(item2);
        this.answerModel.Questionlist.push(question);
        var detail2 = this.questions.filter(y => y.Id == id);
        var detail = detail2[0];
        switch (order) {
          case 1:
            detail.AnswerOrder1 = 1;
            break;
          case 2:
            detail.AnswerOrder2 = 1;
            break;
          case 3:
            detail.AnswerOrder3 = 1;
            break;
          default:
            detail.AnswerOrder4 = 1;
        }
      }

    }
    else {
      var detail2 = this.questions.filter(y => y.Id == id);
      var detail = detail2[0];
      if (orderSequence == 4) {
        detail.AnswerOrder1 = detail.AnswerOrder1 == orderSequence ? null : (detail.AnswerOrder1);
        detail.AnswerOrder2 = detail.AnswerOrder2 == orderSequence ? null : detail.AnswerOrder2;
        detail.AnswerOrder3 = detail.AnswerOrder3 == orderSequence ? null : detail.AnswerOrder3;
        detail.AnswerOrder4 = detail.AnswerOrder4 == orderSequence ? null : detail.AnswerOrder4;
      }
      if (orderSequence == 1) {
        detail.AnswerOrder1 = detail.AnswerOrder1 == orderSequence ? null : (detail.AnswerOrder1 > 1 ? (detail.AnswerOrder1 - 1) : null);
        detail.AnswerOrder2 = detail.AnswerOrder2 == orderSequence ? null : (detail.AnswerOrder2 > 1 ? (detail.AnswerOrder2 - 1) : null);
        detail.AnswerOrder3 = detail.AnswerOrder3 == orderSequence ? null : (detail.AnswerOrder3 > 1 ? (detail.AnswerOrder3 - 1) : null);
        detail.AnswerOrder4 = detail.AnswerOrder4 == orderSequence ? null : (detail.AnswerOrder4 > 1 ? (detail.AnswerOrder4 - 1) : null);
      }
      if (orderSequence == 2) {
        detail.AnswerOrder1 = detail.AnswerOrder1 == orderSequence ? null : (detail.AnswerOrder1 > 2 ? (detail.AnswerOrder1 - 1) : detail.AnswerOrder1);
        detail.AnswerOrder2 = detail.AnswerOrder2 == orderSequence ? null : (detail.AnswerOrder2 > 2 ? (detail.AnswerOrder2 - 1) : detail.AnswerOrder2);
        detail.AnswerOrder3 = detail.AnswerOrder3 == orderSequence ? null : (detail.AnswerOrder3 > 2 ? (detail.AnswerOrder3 - 1) : detail.AnswerOrder3);
        detail.AnswerOrder4 = detail.AnswerOrder4 == orderSequence ? null : (detail.AnswerOrder4 > 2 ? (detail.AnswerOrder4 - 1) : detail.AnswerOrder4);
      }
      if (orderSequence == 3) {
        detail.AnswerOrder1 = detail.AnswerOrder1 == orderSequence ? null : (detail.AnswerOrder1 > 3 ? (detail.AnswerOrder1 - 1) : detail.AnswerOrder1);
        detail.AnswerOrder2 = detail.AnswerOrder2 == orderSequence ? null : (detail.AnswerOrder2 > 3 ? (detail.AnswerOrder2 - 1) : detail.AnswerOrder2);
        detail.AnswerOrder3 = detail.AnswerOrder3 == orderSequence ? null : (detail.AnswerOrder3 > 3 ? (detail.AnswerOrder3 - 1) : detail.AnswerOrder3);
        detail.AnswerOrder4 = detail.AnswerOrder4 == orderSequence ? null : (detail.AnswerOrder4 > 3 ? (detail.AnswerOrder4 - 1) : detail.AnswerOrder4);
      }

      var data = this.answerModel.Questionlist.find(x => x.id == id);
      if (data.AnswerItems.length == 1) {
        var index = this.answerModel.Questionlist.indexOf(data);
        if (index > -1) {
          this.answerModel.Questionlist.splice(index, 1);
        }
      }
      else {
        var subdata = data.AnswerItems.find(x => x.AnwserCode == event.target.id)
        var subdataindex = data.AnswerItems.indexOf(subdata);
        if (subdataindex > -1) {
          data.AnswerItems.splice(subdataindex, 1);
        }

      }



    }


  }
  Reset(id: any, event: any) {
    var data = this.answerModel.Questionlist.find(x => x.id == id);
    var index = this.answerModel.Questionlist.indexOf(data);
    if (index > -1) {
      this.answerModel.Questionlist.splice(index, 1);

    }
  }
  Submit() {
    if (this.answerModel.Questionlist.length < 20) {
      this.errorMeassage = "Please attempt all questions with all options to get accurate result.";
    }
    else {
      const detail = this.answerModel.Questionlist.find(x => x.AnswerItems.length < 4);
      if (detail != undefined || detail != null) {
        this.errorMeassage = "Please attempt all questions with all options to get accurate result.";
      }
      else {
        this.loading = true;
        this.errorMeassage = "";
        var UserId = parseInt(localStorage.getItem("temp"));
        this.answerModel.Questionlist.forEach(element => {
          element.UserId = UserId;
        });
        this.http.post(ApiUrl.BaseUrl + "Question/addAnswer", this.answerModel).subscribe((res) => {
          this.loading = false;
          if (res == 201) {
            this._route.navigateByUrl('assessment-two');
          }
        },
        ()=> this.loading = false);
      }
    }

  }

}


export class SelectedAnswer {
  public Questionlist: QuestionsAnswer[] = [];
}
export class QuestionsAnswer {

  constructor() {
    this.UserId = 0;
  }
  id: number;
  UserId: number;

  AnswerItems: AnswerItem[] = [];
}
export class AnswerItem {
  AnwserCode: string;
  order: number;
}


interface Question {
  id: number;
  question: string;
  answer1: string;
  answerCode1: string;
  answer2: string;
  answerCode2: string;
  answer3: string;
  answerCode3: string;
  answer4: string;
  answerCode4: string;
  isActive: boolean;
  isChecked: boolean;
  AnswerOrder1: string;
  AnswerOrder2: string;
  AnswerOrder3: string;
  AnswerOrder4: string;
}
