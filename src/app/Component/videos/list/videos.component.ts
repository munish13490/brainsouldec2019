import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUrl } from '../../../_services/_apiurl';

@Component({
  selector: 'videos-component',
  templateUrl: './videos.component.html'
})
export class VideosComponent implements OnInit {
  public videos;
  showSaveButton = false;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

  }
  ngOnInit() {
    this.http.get(ApiUrl.BaseUrl + 'Videos/GetVideos').subscribe(result => {
      this.videos = result;
      this.videos.forEach(element => {
        element.IsEdit = false;
      });
    }, error => console.error(error));
  }


  AddVideo(item: any) {
    var data = this.videos.find(x => x.id == item.id).IsEdit = true;
    this.showSaveButton = true;
  }
  Cancel(item) {
    var data = this.videos.find(x => x.id == item.id).IsEdit = false;
    this.showSaveButton = false;
  }
  Save(item) {
    this.http.post(ApiUrl.BaseUrl + "Videos/AddVideo", item).subscribe((res) => {
      if (res == 201) {
        this.showSaveButton = false;
        this.ngOnInit();
      }
    });

  }

}

interface Video {
  id: number;
  url: string;
  name: string;
  isActive: boolean;
  IsEdit: boolean;
}
