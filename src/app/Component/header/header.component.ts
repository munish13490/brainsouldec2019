import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  aboutClick() {
    window.location.href = "/about";
  }
  homeClick(){
    window.location.href = "/home";
  }
  galleryClick(){
    window.location.href = "/gallery";
  }
  contactClick(){
    window.location.href = "/contact";
  }

}
