export class ContactUs {
  public FirstName: string;
  public Age: string;
  public Profession: string;
  public Email: string;
  public Message: string;
  public ContactNumber: string;
}