export class User {
    public Name: string;
    public Gender: string;
    public Mobile: string;
    public Email: string;
    public Age: string;
    public ProfileType: string;
    public BrainTestCode: string;

}