import { DecimalPipe } from "@angular/common";

export class Payment {
    public EnUserId: any;
    public UserId: any;
    public Amount: number;
    public OriginalAmount: number;
}