export class Institute {
    public CreateDate?: Date;
    public Id?: number;
    public ModifiedDate?: Date;
    public Name?: string;
    public TypeId?: number;
    public Url?: string;
}
