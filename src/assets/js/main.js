(function($){"use strict";$('#preloader').fadeOut();$('.counter').counterUp({time:500});$('#portfolio').mixItUp();$('#carousel-screen').carousel({num:5,maxWidth:450,maxHeight:300,distance:50,scale:0.6,animationTime:1000,showTime:4000});var owl=$("#clients-scroller");owl.owlCarousel({items:5,itemsTablet:3,margin:90,stagePadding:90,smartSpeed:450,itemsDesktop:[1199,4],itemsDesktopSmall:[980,3],itemsTablet:[768,3],itemsTablet:[767,2],itemsTabletSmall:[480,2],itemsMobile:[479,1],});var owl=$("#testimonials");owl.owlCarousel({navigation:false,pagination:true,slideSpeed:1000,stopOnHover:true,autoPlay:true,items:1,itemsDesktop:[1199,1],itemsDesktopSmall:[980,1],itemsTablet:[768,1],itemsTablet:[767,1],itemsTabletSmall:[480,1],itemsMobile:[479,1],});var owl=$(".touch-slider");owl.owlCarousel({navigation:false,pagination:true,slideSpeed:1000,stopOnHover:true,autoPlay:true,items:4,itemsDesktopSmall:[1024,4],itemsTablet:[600,2],itemsMobile:[479,1]});$('.touch-slider').find('.owl-prev').html('<i class="lni-arrow-left"></i>');$('.touch-slider').find('.owl-next').html('<i class="lni-arrow-right"></i>');var owl=$(".screens-slider");owl.owlCarousel({navigation:false,pagination:true,slideSpeed:1000,stopOnHover:true,autoPlay:true,addClassActive:true,items:3,itemsDesktopSmall:[1024,3],itemsTablet:[600,1],itemsMobile:[479,1]});$(window).on('scroll',function(){if($(window).scrollTop()>100){$('.header-top-area').addClass('menu-bg');}else{$('.header-top-area').removeClass('menu-bg');}});$('.video-popup').magnificPopup({disableOn:700,type:'iframe',mainClass:'mfp-fade',removalDelay:160,preloader:false,fixedContentPos:false,});var offset=200;var duration=500;$(window).scroll(function(){if($(this).scrollTop()>offset){$('.back-to-top').fadeIn(400);}else{$('.back-to-top').fadeOut(400);}});$('.back-to-top').on('click',function(event){event.preventDefault();$('html, body').animate({scrollTop:0},600);return false;})
new WOW().init();$(window).on('load',function(){$('body').scrollspy({target:'.navbar-collapse',offset:195});$(window).on('scroll',function(){if($(window).scrollTop()>100){$('.fixed-top').addClass('menu-bg');}else{$('.fixed-top').removeClass('menu-bg');}});});function close_toggle(){if($(window).width()<=768){$('.navbar-collapse a').on('click',function(){$('.navbar-collapse').collapse('hide');});}
else{$('.navbar .navbar-inverse a').off('click');}}
close_toggle();$(window).resize(close_toggle);}(jQuery));

var TxtRotate = function (el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
  };

  TxtRotate.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
      this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

    var that = this;
    var delta = 300 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
      delta = this.period;
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
      this.isDeleting = false;
      this.loopNum++;
      delta = 500;
    }

    setTimeout(function () {
      that.tick();
    }, delta);
  };

  window.onload = function () {
    var elements = document.getElementsByClassName('txt-rotate');
    for (var i = 0; i < elements.length; i++) {
      var toRotate = elements[i].getAttribute('data-rotate');
      var period = elements[i].getAttribute('data-period');
      if (toRotate) {
        new TxtRotate(elements[i], JSON.parse(toRotate), period);
      }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
    document.body.appendChild(css);
  };